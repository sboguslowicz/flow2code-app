import React from 'react';
import { Text, Image, View, StyleSheet } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

export default class FilmScreen extends React.Component {
    static navigationOptions = {};

    constructor(props) {
        super(props);
        const { navigation } = this.props;
        this.state = {
            data: navigation.getParam("data"),
            country: "",
            genre: "",
        };
        console.log(this.state.data)
    }

    componentDidMount() {
        var xhr = new XMLHttpRequest()

        xhr.onreadystatechange = (e) => {
            if (xhr.readyState !== 4) {
                return;
            }

            if (xhr.status === 200) {
                this.displayData(xhr.response);
            }
        };

        xhr.open('GET', "https://api.themoviedb.org/3/movie/" + this.state.data.id + "?api_key=1f2f7114da6d3b5bca32d844fed2d223&language=pl-PL");

        xhr.send();
    }

    displayData = (data) => {
        var content = JSON.parse(data);

        var filmGenre = "";

        for(var i = 0; i < content.genres.length; i++) {
            filmGenre += content.genres[i].name;

            if(i < content.genres.length - 1) {
                filmGenre += ", "
            }
        }

        var countries = "";

        for(var i = 0; i < content.production_countries.length; i++) {
            countries += content.production_countries[i].name;

            if(i < content.production_countries.length - 1) {
                countries += ", "
            }
        }
        
        this.setState({
            genre: filmGenre,
            country: countries,
        });
    }

    render() {
        return (
            <ScrollView>
                <View style={styles.posterAndTitle}>
                    <Image style={styles.image} source={{ uri: "https://image.tmdb.org/t/p/w200/" + this.state.data.poster_path }} />
                    <View style={{flexDirection: "column"}}>
                        <Text style={{ fontSize: 25, fontWeight: "bold" }}>
                            {this.state.data.title}
                        </Text>
                        <Text>
                            {this.state.data.release_date + "\n" +
                            this.state.country + "\n" +
                            this.state.genre}
                        </Text>
                    </View>
                </View>
                <View style={styles.description}>
                        <Text style={{textAlign: "justify"}}>
                            {this.state.data.overview}
                        </Text>
                    </View>
            </ScrollView>
        )
    }
}

var styles = StyleSheet.create({
    image: {
        marginBottom: 10,
        marginRight: 10,
        height: 250,
        width: 200,
        resizeMode: "contain"
    },
    posterAndTitle: {
        flexDirection: "row",
        marginRight: 10,
        marginVertical: 10,
    },
    description: {
        margin: 10,
    }
})