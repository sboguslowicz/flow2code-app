import React from 'react';
import { TouchableOpacity, Text, TextInput, View, StyleSheet, Image, ImageStore } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

export default class SearchListScreen extends React.Component {
    static navigationOptions = { title: "Wyszukaj film"};

    constructor(props) {
        super(props);
        this.state = {
            results: [],
        };
    }

    goToFilm = (film) => {
        this.props.navigation.navigate('Film', {data: film})
    }

    getFilmsList(input) {
        var xhr = new XMLHttpRequest()

        xhr.onreadystatechange = (e) => {
            if(xhr.readyState !== 4) {
                return;
            }

            if(xhr.status === 200) {
                this.displayData(xhr.response);
            }
        };

        xhr.open('GET', "https://api.themoviedb.org/3/search/movie?api_key=1f2f7114da6d3b5bca32d844fed2d223&query=" 
            + input 
            + "&page=1&include_adult=false&language=pl-PL");

        xhr.send();
    }

    displayData = (data) => {
        var results = (JSON.parse(data)).results;
        var list = [];

        for(var i = 0; i < results.length; i++) {
            console.log(results[i].poster_path);
            list.push(
                <TouchableOpacity style={styles.listElement} key={i} onPress={this.goToFilm.bind(this, results[i])}>
                    <Image style={styles.image} source={{uri: "https://image.tmdb.org/t/p/w200/" + results[i].poster_path}}/>
                    <Text>{"Tytuł: " + results[i].title + "\nPopularność: " + results[i].popularity + "\nLiczba głosów: " + results[i].vote_count}</Text>
                </TouchableOpacity>
            )
        }

        this.setState({
            results: list,
        });
    }

    handleTitle = (input) => {
        this.getFilmsList(input);
    }

    render() {
        return (
            <View>
                <TextInput style={{ margin: 10, fontSize: 18, color: 'black' }}
                    multiline={false}
                    placeholder='Tytuł'
                    placeholderTextColor='grey'
                    onChangeText={this.handleTitle} />
                <ScrollView style={styles.list}>
                    {this.state.results}
                </ScrollView>
            </View>
        )
    }
}

var styles = StyleSheet.create({
    list: {
        marginTop: 10,
        marginHorizontal: 10,
        marginBottom: 50,
        borderTopColor: 'grey',
        borderTopWidth: 1
    },
    listElement: {
        marginTop: 10,
        marginHorizontal: 3,
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
        flexDirection: "row",
    },
    image: {
        marginBottom: 10,
        marginRight: 10,
        height: 150,
        width: 100,
        resizeMode: "contain"
    },
})