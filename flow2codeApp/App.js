import React from 'react';
import { View } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import SearchListScreen from './screens/SearchList';
import FilmScreen from './screens/Film';

const AppNavigation = createStackNavigator({
  SearchList: {
    screen: SearchListScreen,
  },
  Film: {
    screen: FilmScreen,
  },
},
  {
    initialRouteName: 'SearchList'
  }
);

const AppContainer = createAppContainer(AppNavigation);

export default class App extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <AppContainer />
      </View>
    );
  }
};
